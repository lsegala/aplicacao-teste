package br.edu.lsegala.aplicacaoteste;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/hello"})
public class HelloResource {
    @Value("${br.edu.lsegala.greenting}")
    private String greetingWord;

    @GetMapping
    public String sayHello(){
        return String.format("%s!", greetingWord);
    }
}
